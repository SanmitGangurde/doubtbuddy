<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questions\CreateQuestionsRequest;
use App\Http\Requests\Questions\UpdateQuestionsRequest;
use App\Models\Question;
use Illuminate\Routing\Controllers\HasMiddleware;
use Illuminate\Routing\Controllers\Middleware;
use Illuminate\Support\Facades\Gate;

class QuestionsController extends Controller implements HasMiddleware
{
    public static function middleware(): array
    {
        return [
            new Middleware('auth', only: ['create', 'store', 'edit', 'update', 'destroy']),
            new Middleware('trackQuestionsViews', only: ['show']),
        ];
    }

    public function index(){
        $questions = Question::with('owner')->latest()->paginate(10);
        return view('doubtbuddy.questions.index', compact(['questions']));
    }

    public function create(){
        return view('doubtbuddy.questions.create');
    }

    public function store(CreateQuestionsRequest $request){
        auth()->user()->questions()->create([
            'title' => $request->title,
            'body'=> $request->body,
        ]);

        session()->flash('success', 'Question has been added successfully!');
        return redirect(route('questions.index'));
    }

    public function edit(Question $question){
        if(! Gate::allows('update', $question)){
            abort(403);
        }
        Gate::authorize('update', $question);
        return view('doubtbuddy.questions.edit', compact(['question']));
    }

    public function update(UpdateQuestionsRequest $request, Question $question){
        Gate::authorize('update', $question);

        $question->update([
            'title' => $request->title,
            'body'=> $request->body,
        ]);

        session()->flash('success', 'Question has been updated successfully!');
        return redirect(route('questions.index'));
    }

    public function destroy(Question $question){
        Gate::authorize('update', $question);

        $question->delete();

        session()->flash('success', 'Question has been deleted successfully!');
        return redirect(route('questions.index'));
    }

    public function show(Question $question){
        return view('doubtbuddy.questions.show', compact(['question']));
    }
}
